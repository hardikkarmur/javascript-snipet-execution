const express = require('express');
const bodyParser = require('body-parser');

// Create Express Application Server
const app = express();
app.use(compression());
app.use(bodyParser.json({limit: '100mb', extended: true, parameterLimit: 10000000 }));
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true, parameterLimit: 10000000 }));

app.get('/', (req, res) => {
    const data = req.params.data;
	var func = function(string) {
		return (new Function( 'return ' + string + '' )());
	}
	res.send(func(data));
});

let HTTP_PORT = 1505;
var http = require('http');
var httpServer = http.createServer(app).listen(HTTP_PORT);
console.log('Server 1 Listening TO HTTP ON PORT ' + HTTP_PORT);
module.exports = app;
