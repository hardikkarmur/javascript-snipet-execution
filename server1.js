const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios').default;

// Create Express Application Server
const app = express();
app.use(compression());
app.use(bodyParser.json({limit: '100mb', extended: true, parameterLimit: 10000000 }));
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true, parameterLimit: 10000000 }));

app.get('/', async (req, res) => {
	const snipet = req.params.data;
    const server2 = require('./server2.js');
	const result = await axios.get(`http://localhost:1505/${snipet}`);
	res.send(result.data);
});

let HTTP_PORT = 1504;
var http = require('http');
var httpServer = http.createServer(app).listen(HTTP_PORT);
console.log('Server 1 Listening TO HTTP ON PORT ' + HTTP_PORT);
module.exports = app;
